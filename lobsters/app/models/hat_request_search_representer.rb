require 'roar/json'

class HatRequestSearchRepresenter < OpenStruct
  include Roar::JSON
  property :subject
  property :status
end

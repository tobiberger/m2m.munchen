require 'roar/json'

class HatRequestRepresenter < OpenStruct
  include Roar::JSON
  property :subject
  property :item
  collection :evidence
end

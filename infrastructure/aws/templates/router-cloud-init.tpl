#cloud-config
package_upgrade: true

packages:
  - awscli
  - nodejs

runcmd:
  - AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_service_bucket}/${router_tarball}" /var/lib/router/router.tgz
  - su -l -c "tar xzf /var/lib/router/router.tgz" ubuntu
  - su -l -c "cd m2m.router && LOBSTERS_DNS_NAME=http://${lobster_dns_name} MODLOG_DNS_NAME=http://${modlog_dns_name} ZIPKIN_BASE_URL=http://${zipkin_hostname} node index.js" ubuntu
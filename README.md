[![pipeline status](https://gitlab.com/tobiberger/m2m.munchen/badges/master/pipeline.svg)](https://gitlab.com/tobiberger/m2m.munchen/commits/master)
# Monolith-to-Microservices Welcome

This directory is where you will do all your work for this class. In the beginning, it contains only a little bit of code and infrastructure scripting. By the end of the week, it will be home to multiple services and their deployment configurations.

## Getting Set up

There are a few requirements for everything to work.

1. You must have Docker installed and working. Visit [Docker.com](https://docker.com) to download the Community Edition. It is called "Docker CE". You do not need Docker Enterprise for this class.
2. You must have the AWS command line tools installed. See [AWS Command Line Interface](https://aws.amazon.com/cli/) for instructions to download and set up these tools.
3. You must have an AWS account that is allowed to create VPCs, RDS instances, S3 buckets, and all EC2 resources. If you create a new account, be sure to attach a credit card to it or AWS will not allow you to create enough EC2 instances. The total cost for AWS resources used during this class will be about US $25.
4. You must create an account on [Gitlab.com](https://gitlab.com). A free account will suffice.


## Folders

`lobsters` contains the code and configuration for our monolith. This is a Ruby on Rails application.

`infrastructure` contains configuration scripts and tools we will use to run a local network of services and provision a real production environment on AWS.

## About "Monorepos"

Because we are putting all our services in a single version control repository, this is what's known as a "monorepo."

There is plenty of controversy about whether a monorepo is a good idea or a bad one.

For the purposes of this class we use a monorepo just to make it easier to teach. Whether you want to use a monorepo for your own system is your choice.

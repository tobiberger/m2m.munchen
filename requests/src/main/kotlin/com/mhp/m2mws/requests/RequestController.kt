package com.mhp.m2mws.requests

import org.springframework.hateoas.Link
import org.springframework.hateoas.Resource
import org.springframework.hateoas.Resources
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/request")
class RequestController(private val requestRepository: RequestRepository) {

    private fun createResource(req: Request): Resource<Request> {
        return Resource(req).apply {
            add(linkForCall { getById(req.requestId ?: -1) }.withSelfRel().withType(GET))
            addActionLinks(this)
        }
    }

    @GetMapping(path = ["/{id}"])
    internal fun getById(@PathVariable id: Long): ResponseEntity<Resource<Request>> {
        val request = requestRepository.findById(id)
        return if (request.isPresent) {
            ResponseEntity.ok(this.createResource(request.get()))
        } else {
            ResponseEntity.notFound().build()
        }
    }


    @GetMapping
    internal fun index(): Resources<Resource<Request>> {
        return Resources(requestRepository.findAll().map {
            this.createResource(it)
        })
    }

    @PostMapping
    internal fun add(@RequestBody input: Request): Resource<Request> {
        for (e in input.evidence) {
            e.request = input
        }
        return createResource(requestRepository.save(input))
    }

    @GetMapping("/search")
    fun search(
            @RequestParam("status", required = false) status: Request.Status?,
            @RequestParam("subject", required = false) subject: String?
    ): Resources<Resource<Request>> {
        var matches = (status?.let { requestRepository.findByStatus(it) }
                ?: requestRepository.findAll())

        if (subject != null) {
            matches = matches.filter { it.subject == subject }
        }

        val allRes = matches.map { createResource(it) }
        return Resources(allRes)
    }

    @PostMapping("/{id}/{action}")
    fun action(
            @PathVariable(name = "id") id: Long,
            @PathVariable("action") action: Action,
            @RequestBody notarization: Notarization?
    ): ResponseEntity<Resource<Request>> {
        val request = requestRepository.findById(id).orElse(null)
                ?: return ResponseEntity.notFound().build()

        if (!request.isOpen) return ResponseEntity.status(HttpStatus.FORBIDDEN).build()

        return request
                .let {
                    it.perform(action)
                    it.notarization = notarization
                    requestRepository.save(it)
                }
                .let { ResponseEntity.ok(createResource(it)) }
    }

    companion object {

        private const val GET = "GET"
        private const val POST = "POST"

        private fun addActionLinks(resource: Resource<Request>) {
            if (resource.content.isOpen) {
                Action.values().forEach { a ->
                    resource.add(actionLink(a, resource))
                }
            }
        }

        private fun actionLink(action: Action, resource: Resource<Request>): Link {
            return linkForCall {
                action(resource.content.requestId ?: -1, action, null)
            }.withRel(action.id).withType(POST)
        }

        private fun linkForCall(call: RequestController.() -> Any?) = linkTo(methodOn(RequestController::class.java).call())

    }

}

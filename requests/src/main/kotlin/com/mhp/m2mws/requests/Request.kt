package com.mhp.m2mws.requests

import java.util.HashSet
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.OneToOne

@Entity
class Request(
        @Id
        @GeneratedValue
        val requestId: Long? = null,

        val subject: String,

        var item: String,

        @OneToMany(cascade = [CascadeType.ALL], mappedBy = "request", fetch = FetchType.EAGER)
        val evidence: Set<Evidence> = HashSet(),

        var status: Status = Status.pending,

        @OneToOne(cascade = [CascadeType.ALL])
        var notarization: Notarization? = null
) {

    val isOpen: Boolean get() = status == Status.pending

    // Added for 013.request-with-status
    enum class Status {
        pending, approved, rejected
    }

}


package com.mhp.m2mws.requests

import org.springframework.hateoas.Identifiable

enum class Action(private val targetStatus: Request.Status) : Identifiable<String> {

    approve(Request.Status.approved),
    reject(Request.Status.rejected);

    operator fun invoke(r: Request) = r.apply { status = targetStatus }

    override fun getId(): String {
        return this.name
    }

}

fun Request.perform(action: Action) = action(this);

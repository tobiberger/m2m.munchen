package com.mhp.m2mws.requests

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class Evidence(
        @Id
        @GeneratedValue
        val evidenceId: Long? = null,

        @JsonIgnore
        @ManyToOne
        var request: Request? = null,

        var link: String? = null,

        // Needed because "text" is a reserved word in SQL
        @Column(name = "`text`")
        var text: String? = null
)

package com.mhp.m2mws.requests

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Notarization(
        @Id
        @GeneratedValue
        val notarizationId: Long? = null,

        val principal: String,
        val reason: String
)